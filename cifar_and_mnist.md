# Tutorial: CIFAR and MNIST, the Hello World! of CNNs
Let's start training a model over two of the most well known datasets: [CIFAR10](https://www.cs.toronto.edu/~kriz/cifar.html) and [MINIST](http://yann.lecun.com/exdb/mnist/).  
CIFAR10 is a small dataset which uses 32x32 images from 10 classes;  
MNIST is a handwriten digits dataset.  
They are widely used and available, PyTorch through the `torchvision.datasets` library offers a repository from where to download them.  

The most know net that addresses the classification for MNIST dataset is [LeNet](http://yann.lecun.com/exdb/lenet/) and it performs on CIFAR10 as well.  

## LeNet example
![leNet](cifar_and_mnist/leNet.png)


There are multiple versions of LeNet, the above version is composed by two Convolutional (Conv) layers and two Fully Connected (FC) layers separated by Maxpool layers. 

The layers' characteristics are summarized below:  

| Layer        | Input Features Maps | Output Features Maps | input size | output size |
|--------------|:-------------------:|:--------------------:|:----------:|:-----------:|
| Conv0        | 3                   | 8                    | 32x32      | 28x28       |
| Relu         | 8                   | 8                    | 28x28      | 28x28       |
| Maxpool0     | 8                   | 8                    | 28x28      | 14x14       |
| Conv1        | 8                   | 16                   | 14x14      | 10x10       |
| Relu         | 16                  | 16                   | 10x10      | 10x10       |
| Maxpool1     | 16                  | 16                   | 10x10      | 5x5         |
| FC0          | 400                 | 120                  | 1x1        | 1x1         |
| Relu         | 120                 | 120                  | 1x1        | 1x1         |
| FC1          | 120                 | 10                   | 1x1        | 1x1         |

The Conv layers does not apply a zeropadding. The last layer will be a softmax layer that will provide the class percentages.

## ALOHA Gridsearch
The above net topologies is only one of the different topologies which can address the classification task for the CIFAR10 or MNIST datasets. ALOHA can explore those various topologies and shows which performs better.  
ALOHA integrates a **GRIDSEARCH engine**: you provide it with a JSON file describing the design space you want to explore and it creates a set of nets.  
ALOHA uses the interchange data format [ONNX](https://onnx.ai) to store the models either trained or not. The GRIDSEARCH engine creates ONNX files ready to be trained in the next steps.
The JSON below describes a generic design space:  
```
{
  "CH":3,
  "H":128,
  "W":128,
  "CL":20,
  "KS":"3x3",
  "softmax":false,
  "yolo":false,
  "batch_norm":true,
  "boxes":5,
  "double_conv":false,
  "convLayers":[5,4],
  "fcLayers":[1],
  "poolCadence":[2,3],
  "channels_rule":[[2,2,2,2],[2,1,2]],
  "Mvalues":[16,32],
  "Jvalues":[512,1024]
}
```
The parameters are explained [here](https://gitlab.com/aloha.eu/gridsearch/-/blob/unica/README.md#gridsearch-parameters)  

In our first experiment let's focus on a subset of these parameters:

```
{
  "CH":1,
  "H":32,
  "W":32,
  "CL":10,
  "KS":"5x3",
  "convLayers":[2],
  "fcLayers":[2],
  "Mvalues":[8],
  "Jvalues":[512]
}
```
The JSON above describes *exactly* one model, sigtly different from the one in the example above. 

| Layer        | Input Features Maps | Output Features Maps | input size | output size |
|--------------|:-------------------:|:--------------------:|:----------:|:-----------:|
| Conv0        | 3                   | 8                    | 32x32      | 32x32       |
| Relu         | 8                   | 8                    | 32x32      | 32x32       |
| Maxpool0     | 8                   | 8                    | 32x32      | 16x16       |
| Conv1        | 8                   | 16                   | 16x16      | 16x16       |
| Relu         | 16                  | 16                   | 16x16      | 16x16       |
| Maxpool1     | 16                  | 16                   | 16x16      | 8x8         |
| FC0          | 1024                | 512                  | 1x1        | 1x1         |
| Relu         | 512                 | 512                  | 1x1        | 1x1         |
| FC1          | 512                 | 10                   | 1x1        | 1x1         |

We modified the original structure of LeNet to have a simpler model which can be generated by a regular gridsearch.

We'd like to extend a bit our design space in order to find whether there is a model that performs better with a few different characteristc.

```
{
  "CH":1,
  "H":32,
  "W":32,
  "CL":10,
  "KS":"3x3",
  "convLayers":[2,3],
  "fcLayers":[1,2],
  "Mvalues":[8,16],
  "Jvalues":[512]
}
```
The new JSON will create a design space with a total of 6 models, that cover the combinations of 2 or 3 Conv Layers, 1 or 2 FC Layers. The first Conv layer will start with 8 or 16 output features maps.

## Pre-processing
Most of the times the input samples need to be pre-processed. Pre-processing is data-dependent for example you may need to scale the images to a commond size or to normalize the audio volume or to apply some data augmentation and so on. 
The ALOHA Toolflow [integrates a mechanism](pipeline.md) to define and apply pre-configured pre-processing pipelines or you own pipelines.  
Since CIFAR10 and MNIST are ready to use datasets we need a really simple pipeline:  
```
{
    "title" : "CIFAR pipeline",
    "version" : 1,
    "preset" : true,
    "config" : [ 
        {
            "column" : "sample_transforms",
            "plugins" : []
        }, 
        {
            "column" : "dataset_transforms",
            "plugins" : []
        }, 
        {
            "column" : "batch_transforms",
            "plugins" : [ 
                {
                    "name" : "To tensor",
                    "module" : "torchvision.transforms",
                    "class" : "ToTensor",
                    "version" : 1,
                    "validation" : true,
                    "parameters" : {}
                }
            ]
        },
    ]
}
```  
A pipeline is a sequence of operation that will be performed.
Each operation is implemented as a [plugin](https://gitlab.com/aloha.eu/plugins) and you can implement your own plugin to pre process your data.  
CIFAR10 is provided ready to use so the only thing you have to add is a pluging in the third column. Add the 'toTensor' plugin and save the pipeline.
See the tutorial "[How to create plugins and pipelines](pipeline.md)" for more information.

Now we have all we need to start an experiment:
- the dataset (ALOHA will download MNIST or CIFAR10 from the PyTorch repository)
- the models (the description of the design space)
- the pre-processing pipeline 

## Creating a new experiment
We assume the whole toolflow is [correctly installed and working](https://gitlab.com/aloha.eu/aloha_toolflow/-/blob/master/README.md). 
After logged-in click on the button `New project` on the right corner of the ALOHA board.  

The view is composed by a number of sections:
- **Project details**: you can choose a title, a description and a group of users you want to share the project with. 
- **Module selection**: the first step of the flow allow you to use four module: Performance evaluation, Training engine, Security evaluation, Parsimonious refinement
- **Architecture description**: this section enable you to load a JSON file that describes the target architecute
- **Algorithm parameters**: the algorithms can be produced using the GRIDSEARCH engine or a GENETIC ALGORITHM. Alternatively you can provide existing algorithms
- **Constraints**: when needed you can filter out the algorithms which metrics do no respect the thresholds listed in this section.
- **Toolflow settings**: in this section you can choose a preset configuration. Here is also the place where to choose the dataset, point out the folder where it is stored.
- **Learning settings**: this section enables you to define the hyperparameters for the training.
- **Preprocessing**: the preprocessing is composed by the operations performed over the samples of the dataset before they inputs the CNN



For this first tutorial we are going to use only the training engine  
![modules_only_training](cifar_and_mnist/modules_only_training.png)  
we do not take care of the constraints and of the architecture description.

From the toolflow presets field select `Default`. Automatically a number of fied will change value.

You can customize the learning  settings, for example to have better results change the number of epochs to 15.  


Now you have to define which preprocessing should be done over the input samples choosing a pipeline. 
You can choose an already existing pipeline or you can create a new one or you can load one in JSON format from the `Advanced setting` page.

Finally choose the gridsearch core from the algorithms parameters section. You have two ways to set the gridsearch parameters:  
1. select GS Preset1. Now click `Advanced settings` and edit the fields for the gridsearch using the values we defined previously.
2. Create a JSON file, fill it with the description defined early and save it inside the folder `shared_data/gridsearch/gridsearch.json`. 
In the second dropdown menu choose `Get from gridsearch/gridsearch.json`  
![select_gridsearch](cifar_and_mnist/select_gridsearch.png)


Now your configuration is ready and you can press the submit button.

## Starting the experiment
By clicking the button `Submit` a new card will appear in the first column of the board.  
To start the experiment cick on `Start`. Since you've selected just the training engine only the training will be performed.  
ALOHA will download CIFAR10 from the repository, will generate the models accordlying to the gridsearch parameters and will train them.

You can see the training progress trhoug Tensorboard by clicking the icon in the project card.

![tb_example](cifar_and_mnist/tb_example.png)

The training fase will take a while depending on the machine ALOHA is running on.

Once all the algorithms are trained the status of the card will change and a button `View results` will appear.

## Browsing the results
Once the training is finished you can browse the results.  
![results](cifar_and_mnist/results.png)
Each dot represents a trained model. Since we didn't evaulate the performace they are centered in the zero in each plot but they differ for the accuracy. You can select the most accurate one by clicking on `Select this model`.

## Code Generation

After you select an algorithm clik the button `Move to right`  
![card](cifar_and_mnist/card.png)  

You can skip the `System level configuration` step: click again `Move to right`
Now the project is in the `Deployment` step  

This step enables you to automatically create the code to **run the Net** over a specific target. The available targets are:
- Any computer using a C based application
- Any computer using Python and PyTorch
- An ARM based system using a C based application
- A Xilinx device of the Zynq or Ultrascale family on which to program the NEURAghe accelerator.  
![deployment](cifar_and_mnist/deployment.png)  
Select the target you prefer, click on generate and then on download.
The easiest way is to use PyTorch.  


DONE! You model is ready to deploy!

