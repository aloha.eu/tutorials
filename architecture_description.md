# Tutorial: How to create an architecture description for your project

The ALOHA Toolflow is able to select the best models that fits the provided architecture description. The steps which use the architecture description are two:  
- The `Performance Evaluation`, during the design space exploration
- The `System-Level Mapping` performed by SESAME

The `Performance Evaluation` requires a JSON file. This file lists the processing core, the peak perfomance of each,
the supported operations, the connections between cores and memory and the bandwidth.
[Here is an example](https://gitlab.com/aloha.eu/example_data/-/blob/master/architectures/arch.json)

The results of the evaluation are compared to the constraints and, if required, the generated models will be filtered before the training and the other steps in order to save time.

The `System-Level Mapping` uses a *package* which contains service files used by SESAME for the simulation.

The package should contain a JSON file for the `perfomance evaluation` and a folder for the mapping tool SESAME.

```
.
├── architecture_model_1
│   ├── architecture_description.json
│   └── model_sesame
│       ├── arch
│       │   ├── model_sesame_arch.yml
│       │   ├── model_sesame.pi
│       │   ├── other_files
│       │   ├── other_files
│       │   ├── other_files
│       ├── Makefile
│       └── model_sesame.yml
├── architecture_model_2
│   ├── architecture_description.json
│   └── model_sesame
│       ├── arch
│       │   ├── model_sesame_arch.yml
│       │   ├── model_sesame.pi
│       │   ├── other_files
│       │   ├── other_files
│       │   ├── other_files
│       ├── Makefile
│       └── model_sesame.yml
└── architecture_model_3
    ├── architecture_description.json
    └── model_sesame
        ├── arch
        │   ├── model_sesame_arch.yml
        │   ├── model_sesame.pi
        │   ├── other_files
        │   ├── other_files
        │   ├── other_files
        ├── Makefile
        └── model_sesame.yml

```
The string `model_sesame` in the filenames and inside some file will be automatically replaced with the application name by ALOHA during the process. Please preserve it.


The package contains, among the others, the file `model_sesame_arch.yml` where is defined which cores will excecute the specific operations using the sintax:  
```
<property name="operations" value="['ReLU', 'MaxPool']"/>
```

Which means that the core will executes ReLU and MaxPool operations but not Conv.

[Here is an example](https://gitlab.com/aloha.eu/aloha_shared_data/-/blob/master/architectures/sesame_architecture_demo/SESAME_architecture_modelling.md)








