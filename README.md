# ALOHA Tutorials

Learn how to use the ALOHA Toolflow to get your CNN.
## [Install and configure ALOHA Toolflow](https://gitlab.com/aloha.eu/aloha_toolflow/-/blob/master/README.md)  
![ALOHA_architecture](main/ALOHA_architecture.png)  


## [CIFAR10 and MNIST, the Hello World! of CNNs](cifar_and_mnist.md)  
![cifar_mnist](main/cifar_mnist.png)  


## [How to create a dataset](dataset.md)
![dataset](main/dataset.png)  


## [How to create an architecture description](architecture_description.md)  
![arch](main/arch.png)  


## [How to create plugins and pipelines](pipeline.md)  
![pipeline](main/pipeline.png)  


## [Keywords spotting use case](kws.md)  
![kws](main/kws.png)  

## [Detection with YOLO-like models](detection.md)  
![yolo](main/yolo.jpeg)  

## [Train UNet for segmentation. The Medical Imaging Use case](segmentation.md)  
![unet](main/unet.png)  
