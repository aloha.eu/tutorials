# Write your own plugins and build your pipeline
A plugin is a function which takes in input a data and returns a tensor. The input data can be a tensor itself or any value (for example a file path).
The ALOHA Toolflow offers a set of ready to use plugins:
```
load_audio_from_wav -- given a path to a file loads an audio sample
load_image_from_file -- given a path to a file loads an image
audio_to_mel -- converts an array of audio samples to a MEL spectrogram
audio_to_mfcc -- converts an array of audio samples to a MEL frequency cepstral coefficients
cut_or_pad_to_length -- cuts an audio to a specific number of samples or adds a padding if the sample it too short
image_resize -- resize and image to the specified sizes
pad_to_square -- given a rectangulare image returns a squared image with the original image centered and a padding to fill the horizontal or vertical stripes
horizontal_flip -- image data augmentation: flip the given image pivoting on the vertica axe
random_image_rotation -- image data augmentation: rotates the image of a random angle
random_image_scale -- image data augmentation: scales the image of a random scale factor
random_horiz_shift -- image data augmentation: shits the image along the horizontal axe
random_pitch -- audio data augmentation: changes pitch
random_speed -- audio data augmentation: changes the speed
random_sum_noise -- audio data augmentation: adds noise
random_time_shift -- audio data augmentation: shifts the audio of a random amount of ms
torch_from_numpy -- converts the input tensor to a numpy tensor
to_tensor -- convert the input tensor to a PyTorch tensor
```
The above list is not complete, other plugins could have been added.

A cascade of plugins is called Pre-processing pipeline. The ALOHA Toolflow subdivides the pipeline in three stages:  
- Sample level (sample_transforms)
- Dataset level (dataset_transforms)
- Batch level (batch_transforms)

The sample level transforms work over the single sample of the dataset, which is the most common case. The applies resizes, pad or they load the data from the file system and so on.  
The Dataset level transforms work over the whole dataset *after* all the samples have been processed. They applies for examples normalization.  

The pre-processing performed by these two stages are stored in the HDF5 file.

The third stage is the batch level. The transforms of this level take place during the training or the inference of the model. These transormation applies the on-the-fly data augmentation and any other transformation needed before input the model (for example the conversion of the input range)

## Import existing pipelines
Existing pipelines, created by other users, can be imported using the GUI. The pipeline must be stored as JSON file.  
* Go to create project;  
* Click the button "Advanced Settings"  
* In the section "Upload pipeline JSON" browse to your file and upload it  
* Refresh the page and the new pipeline should be available in the dropdown menu  
[Here](https://gitlab.com/aloha.eu/aloha_pipelines) you find a collection of pipeline. 

## Plugins

Each plugin is stored in a folder which contains three files:  
```
.
├── config.json
├── plugin_name.py
└── __init__.py
```

The file `__init__.py` is empty and uses to make the folder a python module.  
The file `config.json` describes the plugin  
```
{
	"name": "Amazing plugin",
	"version": 0,
	"description": "It does amazing things",
	"module": "plugins.installed.plugin_name.plugin_name",
	"class": "AmazingPlugin",
	"type": "transform",
	"transforms": ["sample_transforms", "batch_transforms"],
	"author": {
		"name": "Gianfranco Deriu",
		"email": "gianfranco.deriu@unica.it",
		"url": "https://author.website"
	},
	"license": "MIT",
	"homepage": "https://gitlab.com/aloha.eu/plugins",
	"parameters": [{
			"name": "par1",
			"dtype": "int",
			"default": 42
		},
		{
			"name": "par2",
			"dtype": "float",
			"default": 3.1416
		}
	]
}
```
The `transform` field should list which steps of the pipeline the plugin can be used in.
The `parameters` field lists the parameter used to configure the plugin behaviour.

The file `plugin_name.py` is the python class that defines what the plugin does:  

```
#import anything you need

from ..transform_interface import TransformInterface
OK=1
FAIL=0

class AmazingPlugin(TransformInterface):
    
    def __init__(self, par1=None, par2=None):
        self.par1 = par1
        self.par2 = par1

    def __call__(self, input):
        
      if not input is None:
        print ("Not so amazing, indeed...")
          
        return OK
      else:
        return FAIL
```

The plugins intended for the `data augmentation` have a bit more complex interface. Let's see the RandomImageScale plugins as an example:  
```
import random
from ..transform_interface import TransformInterface
from skimage.transform import rescale

import numpy as np

class RandomImageScale(TransformInterface):

    def __init__(self, probability=0.5, max_scale_perc = 0.5, sync_code = 0):
        self.probability=probability
        self.max_scale_perc = max_scale_perc;
        self.sync_code = sync_code;

    def __call__(self, image, **kwargs):
        seed = kwargs.get("seed", None)
        if seed is not None:
          seed += self.sync_code
        random.seed(a=seed)
        
        if random.random() < self.probability:
        
            img_size = image.shape[0]
            imagedtype = image.dtype

            scale = random.uniform(1.0 - self.max_scale_perc, 1.0 + self.max_scale_perc)

            image = rescale(
                image,
                (scale, scale),
                multichannel=True,
                preserve_range=True,
                mode="constant",
                anti_aliasing=False,
            )

            if scale < 1.0: # pad remaining part
                diff = (img_size - image.shape[0]) / 2.0
                padding = ((int(np.floor(diff)), int(np.ceil(diff))),) * 2 + ((0, 0),)
                image = np.pad(image, padding, mode="constant", constant_values=0)

            else: # crop exceeding part
                x_min = (image.shape[0] - img_size) // 2
                x_max = x_min + img_size
                image = image[x_min:x_max, x_min:x_max, ...]
            
            image = image.round()
            image = image.astype(imagedtype)
            
            
        return image
```

It defines a parameter named `probability` used to set the strength of the data augmentation perfomed by the plugin. The higher the value the more likely this transformation is applied.  
Another parameter is the `sync` value. It is used to sync two instances of the same plugin: one works for the samples and one for the labels. This is useful in tasks where both samples and label must be preprocessed like Detection and Segmentation.

Finally, the method `__call__` has the argument `**kwargs` which is mainly used to pass a seed to the random generator to sync the transformation of two instances of the same plugin.
The `**kwargs` dictionary could containg any other uselful argument.

Using the above template you can write your own plugins. Several plugins can be cascaded forming a pipeline.

## Pipeline

A cascade of plugins is named Pipeline. A pipeline can be composed in the ALOHA toolflow's GUI or can be defined in a json file like the following:  
```
{
    "title": "6_MelST_32_16_aug",
    "version": 1,
    "preset": true,
    "config": [
        {
            "column": "sample_transforms",
            "plugins": [
                {
                    "name": "Load audio from wav",
                    "module": "plugins.installed.load_audio_from_wav.load_audio_from_wav",
                    "class": "LoadAudioFromWav",
                    "version": 123,
                    "validation": true,
                    "parameters": {
                        "mono": true,
                        "signal_rate": 16000
                    }
                },
                {
                    "name": "Cut or pad to length",
                    "module": "plugins.installed.cut_or_pad_to_length.cut_or_pad_to_length",
                    "class": "CutOrPadToLength",
                    "version": 123,
                    "validation": true,
                    "parameters": {
                        "target_length": 16000
                    }
                }
            ]
        },
        {
            "column": "dataset_transforms",
            "plugins": []
        },
        {
            "column": "batch_transforms",
            "plugins": [
                {
                    "name": "Random speed",
                    "module": "plugins.installed.random_speed.random_speed",
                    "class": "RandomSpeed",
                    "version": 123,
                    "validation": false,
                    "parameters": {
                        "probability": 0.3,
                        "min_speed_coef": 0.9,
                        "max_speed_coef": 1.1,
                        "top_db": 30
                    }
                },
                {
                    "name": "Random pitch",
                    "module": "plugins.installed.random_pitch.random_pitch",
                    "class": "RandomPitch",
                    "version": 123,
                    "validation": false,
                    "parameters": {
                        "probability": 0.3,
                        "sampling_rate": 16000,
                        "min_n_steps": -2,
                        "max_n_steps": 2,
                        "bins_per_octave": 12
                    }
                },
                {
                    "name": "Random time shift",
                    "module": "plugins.installed.random_time_shift.random_time_shift",
                    "class": "RandomTimeShift",
                    "version": 123,
                    "validation": false,
                    "parameters": {
                        "probability": 0.3,
                        "top_db": 30
                    }
                },
                {
                    "name": "Audio to Mel (Reply)",
                    "module": "plugins.installed.audio_to_mel_reply.audio_to_mel_reply",
                    "class": "AudioToMelReply",
                    "version": 123,
                    "validation": true,
                    "parameters": {
                        "signal_rate": 16000,
                        "n_mels": 16,
                        "num_frames": 32,
                        "frame_overlap_ratio": 0.5,
                        "n_fft": null,
                        "power": 2,
                        "htk": true,
                        "norm": null
                    }
                },
                {
                    "name": "To tensor",
                    "module": "torchvision.transforms",
                    "class": "ToTensor",
                    "version": 123,
                    "validation": true,
                    "parameters": {}
                }
            ]
        }
    ]
}
```

The above JSON describes the pipeline in figure  

![kws_pipeline](pipeline/kws_pipeline.png)

The pipeline is intended to preprocess the audio and add some data augmentation.

A pipeline has a title, a version and three lists of plugins. Each list is one of the pre-processing stages described above.


