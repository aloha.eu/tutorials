# Format your dataset for ALOHA toolflow

The ALOHA toolflow can work over three diffent tasks:  
- classification
- object detection
- segmentation (One class over a background)

for each task type a convenient way to format the dataset has been implemented. The dataset information can be set in the Toolflow settings section.  
![dataset_gui](datasets/dataset.png)  

The following fields are available:  
- Dataset name: you can select one of the well known datasets (MNIST, CIFAR10 or CIFAR100), a CUSTOM dataset or an EXISTING dataset.
The known datasets are provided by pythorch so the other fields are not necessary.  
The EXISTING dataset is a HDF5 file already created in a previous project, with an associated json file which defines the splits. 
If a json file is not in the same folder of the HDF5 file a new slit is created using the percentage defined in `split` field.  
The CUSTOM dataset is a collection of samples, labels, annotations and masks.

The other fields are interpreted by the toolflow depending on the task.
- dataset folder
- dataset file
- split
- number of classes

## Classification task
A classification task can be performed over any type of data: images, audio, ECG signals, neural signals and so on. 
The preprocessing of the samples before the CNN is in charge of the pre-processing pipeline. What we show in the following is independet of the pre-processing.  
The custom dataset should be composed by a number of samples and a txt file listing all the samples and the associated labels.
For example the following is a dataset with images for the 4 classes

- dog 0
- cat 1
- car 2
- person 3


The labels file `labels.txt` should look like:
```
samples/image_of_a_dog.png 0
samples/image_of_a_dog4.png 0
samples/image_of_a_person.png 3
samples/image_of_a_cat4.png 1
samples/another_image_of_a_dog.png 0
samples/third_image_of_a_dog.png 0
samples/image_of_a_cat.png 1
samples/another_image_of_a_cat.png 1
samples/third_image_of_a_cat.png 1
samples/another_image_of_a_person.png 3
samples/third_image_of_a_person.png 3
samples/image_of_a_car.png 2
samples/another_image_of_a_car.png 2
samples/image_of_a_car4.png 2
samples/image_of_a_person4.png 3
samples/third_image_of_a_car.png 2
```

If we assume the dataset folder is myDataset the `tree` command should output:
```
$ tree myDataset
.
├── samples
│   ├── image_of_a_dog.png
│   ├── another_image_of_a_dog.png
│   ├── third_image_of_a_dog.png
│   ├── image_of_a_dog4.png
│   ├── image_of_a_cat.png
│   ├── another_image_of_a_cat.png
│   ├── third_image_of_a_cat.png
│   ├── image_of_a_cat4.png
│   ├── image_of_a_car.png
│   ├── another_image_of_a_car.png
│   ├── third_image_of_a_car.png
│   ├── image_of_a_car4.png
│   ├── image_of_a_person.png
│   ├── another_image_of_a_person.png
│   ├── third_image_of_a_person.png
│   ├── image_of_a_person4.png
└── labels.txt

1 directory, 17 files
```

Accordlying to this example the fields data folder and data file should be set to
- data folder: `myDataset`
- data file:   `myDataset/labels.txt`

Since the `labels.txt` file define the relative path starting from the myDataset folder the data folder must be set to the root directory of the dataset. 
If the label.txt file defined only the file names and without a path the data folder had to be myDataset/samples in order to be prepended to the namefile.


The split value defines the number of samples used for the training and the number used for the validation. The samples will be shuffled.
The number of class in this example is 4.

## Object detection task
The object detection requires an **annotation** for each image which is the list of the boxes enclosing the objects. Each object belong to a class.
The annotations must be stored in a JSON file.  
![yolo](datasets/yolo.jpeg)  
Below there is an annotation example which describes a dataset composed by three images stored in the `images/` folder.
The objects are classified in two classes.
The first image has two objects, the second only one and the third has 4 objects.
```
{"source_path": "images/",
 "meta": [
    {
      "frame": "2009_000457.jpg",
      "width": 500,
      "height": 333,
      "objects": [
        {"object_class": 1, "bb": [332.0, 128.0, 500.0, 209.0]},
        {"object_class": 0, "bb": [58.0, 72.0, 168.0, 296.0]}
                 ]
    },
    {
      "frame": "2007_006277.jpg",
      "width": 640,
      "height": 480,
      "objects": [
        {"object_class": 1, "bb": [65.0, 70.0, 447.0, 322.0]}
        ]
    },
     {
      "frame": "2012_003472.jpg",
      "width": 500,
      "height": 333,
      "objects": [
        {"object_class": 0, "bb": [192.0, 69.0, 250.0, 272.0]},
        {"object_class": 0, "bb": [252.0, 181.0, 312.0, 243.0]},
        {"object_class": 1, "bb": [48.0, 70.0, 150.0, 170.0]},
        {"object_class": 1, "bb": [13.0, 23.0, 80.0, 56.0]}
        ]
      }
    ]
}
```
The JSON file must have two fields:  
* `source_path`: the relative path startin from the annotation file where to find the images
* `meta`: the list of the images

Each image is described as follow:  
* `frame`: the path and the name of the image. It must start from the directory pointed by the source_path field
* `width`: the original width of the image
* `height`: the original height
* `objects`: the list of the objects in the image

Finally each object in the list is composed by the following fields:  
* `object_class`: the class of the object
* `bb`: the bounding boxes in the format: `[x1, y1, x2, y2]`

The JSON file could containt other fields that can be used to add optional information.  
In the same folder of the annotation file a txt file for the anchors can be added. The file must have the following format:  
```
0.8041329379132356, 1.6002175863110129
2.079916423550584, 4.273387226180225
3.097959351526205, 8.018583432357596
5.701375035065909, 9.148700603657378
10.127262298551901, 10.902091912272773
```
Each line is an anchor. The name must be `anchors.txt`. If this file is not present the ALOHA Toolflow will use the Tiny YOLO v2 default anchors:  
```
[
 (0.57273, 0.677385),
 (1.87446, 2.06253),
 (3.33843, 5.47434),
 (7.88282, 3.52778),
 (9.77052, 9.16828)
]
```
Another optional file can be present: `class_names.txt`. It stores the mapping of the integer number and the label of each class:  
```
person
car
```
Which means `person` is mapped to the integer 0 and `car` to the integer 1.

[Here](https://gitlab.com/aloha.eu/detection_datasets) you can find the scripts to extract parts of the COCO and VOC datasets, for example only the pictures which contain people and cars.

A balanced dataset is better.

## Segmentation task
The segmentation task requires for eache sample a masks that highlights the object above the background. 
Currently ALOHA support the segmentation of 1 class of objects; it means that the mask will be 0 for each pixel of the background and 1 for each pixel of the object.  
![segmentation_mask](datasets/segmentation_mask.png)  

Every sample file needs a mask which name must be the file name whit the postfix `_mask` appended before the file exention.


Data folder: the path to the root directory of the dataset.
Dataset file: none





