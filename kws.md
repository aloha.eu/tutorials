# Tutorial: Train a classifier for Keywords spotting
Keywords spotting (KWS) is the task for classification of specific words of importance among a set.
Many solution of this problem have been implemented using the [Google Speech Dataset](https://petewarden.com/2018/04/11/speech-commands-is-now-larger-and-cleaner/)

![kws](kws/kws.png)  
This tutorial is based on that dataset as well.  
The ALOHA Toolflow will perform a Neural Architecture Search (NAS) for you.
In this tutorial we are going to use the grisearch approach over multiple degrees of freedom:  
- model depth (number of layers)
- model width (feature maps of each layer)
- sample pre-processing (different preprocesing steps may be applied to the samples)

The Power-Performance tool will estimate the performance of each resulting design point to be compared with the user's constraints.
The models which satisfy the constraints will be trained. 

![design_space](kws/design_space.png)  

## Dataset
First of all we have to prepare the dataset ([see the tutorial](https://gitlab.com/aloha.eu/tutorials/-/blob/master/dataset.md)).
It is a classification task so we need a file listing a pair for each line formed by the path for the sample and the class.
A user may create its own file describing its dataset, for example one may like to have a dataset with only for words.
The ALOHA team wrote a [set of scripts](https://gitlab.com/aloha.eu/smart_industry_use_case/-/tree/master/september_assessment/scripts) to help you creating a dataset. 

When a txt file is ready we can start creating the project. [Here](https://gitlab.com/aloha.eu/kws10) you can download a dataset with 10 commands and the txt file ready for you.

## Creating the project
Start the ALOHA toolflow, open the web GUI, do the login and click over the button `Create new project`.
![new_project](kws/new_project.png)

The GUI offers presets that fill the form for you, just select `KWS preset` to set a default configuration.  
![kws_preset](kws/presets.png)  
You can still customize the paramenters.

We will use this setup:  
```
Module selection
  Performance: on
  Training: on
  Security: on
  Parsimonious: on

Dataset
  Dataset name: CUSTOM
  Dataset folder: usecases/kws10
  Dataset file: usecases/kws10/kws10_small.txt
  Split on train/validation: 0.7
  Number of classes: 10


Learning settings
  Task type: classification
  Mode: Full
  Learning rate: 0.01
  Decay: 0.1
  Iteration: 3
  Epoch wait: 15
  Batch size: 16
  Learning epochs: 25
  Loss function: Cross Entropy
  Optimizer funcion: sgd
  Accuracy: percent

Pre-processing pipeline
  Select the pipeline we suggest in the next paragraphs

Algorithm Parameters
  Gridsearch -> get from gridsearch/gridserch.json

Architecture description
  Choose the architecture file we suggest in the next paragraphs

Contraints
  Power consumption:  12 mJ/query
  Security level: Medium
  Execution time: 500 ms/query
  Memory footprint: 1500 kB
  Accuracy: 60%

  The priority 1 for all of them
```
![create_kws1](kws/create1.png)
![createkws2](kws/create2.png)

### Pre-processing plugins pipeline
[Here](kws/KWSpipeline.json) you can download the pipeline of plugins for the pre-processing and a the data augmentation.
Click the `Advanced settings` button ![advancesettingbutton](kws/advancesettingbutton.png) and upload the pipeline.  
![upload_pipeline](kws/upload_pipeline.png)  
[Here](pipeline.md) is a tutorial to write your own plugins and build your pipeline.

![kws_pipeline](kws/kws_pipeline.png)

### Setup the gridsearch
For the gridsearch parameters we can use this setup:  

```
{
  "CH":1,
  "H":28,
  "W":28,
  "CL":10,
  "KS":"3x3",
  "softmax":false,
  "leakyrelu":false,
  "yolo":false,
  "boxes":5,
  "double_conv":false,
  "convLayers":[2,4],
  "fcLayers":[2,3],
  "Mvalues":[16,32],
  "Jvalues":[64,128]
}
```
[Here](https://gitlab.com/aloha.eu/gridsearch/-/blob/unica/README.md#gridsearch-parameters) is described how to configure your gridsearch.  

### Choose the target architecture
As target architecture we selected a dual core ARM, described by [this file](kws/arch.json).

### Constraints
We'd like to have an execution time less than 100 ms per sample, a memory footprint of 500kB maximum and a power consumption of 100 uJ/sample. Security level medium.


## Start the evaluation
Once you fully compiled the form click on `Submit`. A new card appears in the board and you can click on `Start`.
The evaluation starts, you can follow it in the terminal.
Once the training start, you can see how loss and accuracy are going by clicking on the button that opens the integrated **TensorBoard** page.

![training](kws/training.png)  

Partial result can also be seen in the results tab. Depending on the machine the full evaluation can take a while.

## First step results
After a while the first step is completed. The card status will pass from `Running` to `Finished`.  
![completedfirst](kws/completedfirst.png)  
Clicking the button `View results` will show you the result tab.  
![results](kws/results.png)

The first line shows three pareto plots respectively for `Execution time`, `Power consumption` and `Memory footprint`. Each dot is an algorithm produced by the gridsearch.  
The second line shows two plots the first with a detail on accuracy and loss and second with details on estimated power performance.  
The bottom of the tab shows a table which lists all the algorithms and the results.

Let's comment the pareto plots.

### Dot's position
Each plot has the accuracy as Y axe and should show green area, if not you can click the button `Fit to constraints` to change the zoom.
The dots which fall in the green area respect the chosen constraint.
If a dot falls on the right of the green area it has been discarded *before* the training step in order to save processing time.

### Dot's color
A model designer would like to have a robust model against the attacks of a malicious user of the deployed system. 
There are plenty of studies that show how a model can be misleaded from the extern. The  Security tool of ALOHA evaluates the robusteness of the generated models
and, in a second step, ALOHA can fine-train them using specific loss functions in order to improve the security

The colors of the dots reports the starting security level:
- green  -> high
- yellow -> medium
- red    -> low
- black  -> the algorithm has been discarderd beacuse of performance constraints.

### Dot's size
Finally, the size of the dots is related to the results of the quantization performed by the RPI tool. The smaller the dot the lower the number of the bits.  
![rpi](kws/rpi.png)  
In the above picture is plotted the *quantization process*. Starting from 8 bits, on the left, the RPI tool reduces the number of bits used for the representation of 
parameters and activations. After each reduction a step of retraining is performed. This results are also reported on the table with loss and accuracy after each quantization step.


NOTE: It can happen that some algorithm respects the constraints but it hasn't been trained: this is because some problem happened in the training phase, 
i.e. inapropriate LR for the algorithm caused a *NaN* value for the loss function or the net is too deep for the input size of the sample and so on.

Now, you can choose an algorithm by clicking on `Select this algorithm` to go ahead for the second step. ![selectthismodel](kws/selectthismodel.png)


## Architectural mapping

Once you selected the algorithm click the button `Move to >`. The card moves to the second lane an two switchs appear.  
![secondlane](kws/secondlane.png)

In this second step you can perform an architectural mapping which will evaluate what core will excute the specific layer and what performance you should have.
The mapping is performed by the High-level simulative tool **SESAME by UvA**.  
![arch_mapping](kws/arch_mapping.png)  

You can also perform the **Parsimonius Inference** (PI), it will process the net to optimize it in order to save execution time in inference.
Let's select just Sesame and click on the `Start` button.

After a while the results are ready, click on `View results` to open the tab.  
![mapping](kws/mapping.png)

On the left it shows two plot with the extimated performance of each mapping.
In the middle, using [Netron](https://github.com/lutzroeder/netron), it shows the model and on the right you can see the produced mappings.

## Code generation
Completed the other step the last thing is to *generate and download* an implementation of the net ready to be deployed.  
![codegen](kws/codegen.png)


Currently the tool supports: 
- **plain C** implementation: it is based on the NEURAghe framework and can be compiled for each general purpose processor.
- **NEURAghe accelerated**: it exploits the acceleretion of the convolutional layers of the NEURAghe accelerator. It can be used on Zynq devices.
- **Pytorch**: the python code
- **ESPAM** implementation: for general pupose processor based on p-threads.

![codegen_download](kws/codegen_download.png)




