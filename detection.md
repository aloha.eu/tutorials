# YOLO-like models for detection.
The detection task requires to spot one or multiple objects in a single image drawing a box around them.  
![yolo](detection/yolo.jpeg)  
The detection can be performed using a classifier:  
- Divide the image in tiles, also overlapping tiles and of different sizes.
- Process each tile with a classifier  able to classify the 
- Take the tiles with best accuracy over a certain threshold. Those are the results of the detection.

This method is actually used but it requires a lot of computation.
A game-changing approach is the one developed by Joseph Redmon: [YOLO (You Only Look Once)](https://pjreddie.com/darknet/yolo/)

The image is processed only one time and the output of the network is a tensor with information about the position, size and class of the boxes that contains the objects of the image.
This tensor is then processed by a Region layer which extracts the information and returns the final results. This layer uses a set of `anchors` also called `priors` by the author.
They are pairs (height, width) which are the more frequent shapes of the objects found in the dataset. Instead of starting from random values for the box sizes tha anchors allow to start from more likely values.
Actually it is reasonable that a person is tall and thin and a car is low and large.  
![anchors](detection/anchors.png)  
YOLO has shown to perform really good and it evolved through the years.

The ALOHA Toolflow uses the YOLO approach to address the detection task. We implemented two ways:  
**The first method** starts with the model of [Tiny YOLO v2 stored in a ONNX file](https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/tiny-yolov2).
The net is slightly modified to fit the number of classes of the specific tasks. Then the model is retrained over the provvided dataset.  
**The second method** follows which the steps to train a YOLO model are: it starts training a classifier, the YOLO author used [Imagenet](http://www.image-net.org/) which is a huge dataset.
Then the FC layers are removed and replaced with the final Convolutional layers which produce the tensor for the Region layer.
Finally the new model is trained over the provided dataset.

The best results have been obtained from the first method which is also faster.
The drawback is that the model size is limited by the size of Tiny YOLO v2 and the classes are those defined for it.  

In the following examples we are going to train a model which detects two classes: `Person` and `Car`.

## Dataset
Before creating a project we need a dataset to train the model on.
[Here](https://gitlab.com/aloha.eu/tutorials/-/blob/master/dataset.md) you can find how the dataset must be prepared for the detection task in ALOHA.

For detection tasks several dataset have been created by researcher, two of them are:
- COCO dataset: https://cocodataset.org/#home
- Pascal VOC dataset: http://host.robots.ox.ac.uk/pascal/VOC/

Pascal VOC has been actually used by the author of YOLO.  
In our experiments we used both the datasets. [Here](https://gitlab.com/aloha.eu/detection_datasets) you can find a script to extract parts of the datasets, for example only the pictures which contain people and cars.
The scripts allows you to choose the number of samples in order to have a smaller dataset for faster experiments (but lower performance).
The scripts also creates a file with the `anchors` used by YOLO and a file listing the objects' classes.

## Pre-processing pipeline
The sample of the dataset must be pre-processed before going as input of the model.
You can define [your own plugins and pipelines](https://gitlab.com/aloha.eu/tutorials/-/blob/master/pipeline.md) to preprocess them or you can get one or more from our [repository](https://gitlab.com/aloha.eu/aloha_pipelines).
The author of YOLO used a simple preprocessing. The sample images must be squared (416px x 416px). In order to not stretch the image a padding is added.  
We already defined four versions of this preprocessing:
- with gray padding
- with white padding
- with black padding
- without padding, so it stretches the image

![detection_preprocessing](detection/detection_preprocessing.png)  

An example:  
```
{
    "title" : "Detection_graypad",
    "version" : 2,
    "preset" : true,
    "config" : [ 
        {
            "column" : "sample_transforms",
            "plugins" : [ 
                {
                    "name" : "Load image from file",
                    "module" : "plugins.installed.load_image_from_file.load_image_from_file",
                    "class" : "LoadImageFromFile",
                    "version" : 0,
                    "validation" : true,
                    "parameters" : {
                        "grayscale" : false
                    }
                }, 
                {
                    "name" : "Pad to square",
                    "module" : "plugins.installed.pad_to_square.pad_to_square",
                    "class" : "PadToSquare",
                    "version" : 1,
                    "validation" : true,
                    "parameters" : {
                        "pad_value" : 128
                    }
                }, 
                {
                    "name" : "Image resize",
                    "module" : "plugins.installed.image_resize.image_resize",
                    "class" : "ImageResize",
                    "version" : 0,
                    "validation" : true,
                    "parameters" : {
                        "H" : 416,
                        "W" : 416
                    }
                }
            ]
        }, 
        {
            "column" : "dataset_transforms",
            "plugins" : []
        }, 
        {
            "column" : "batch_transforms",
            "plugins" : [ 
                {
                    "name" : "To tensor",
                    "module" : "torchvision.transforms",
                    "class" : "ToTensor",
                    "version" : 123,
                    "validation" : true,
                    "parameters" : {}
                }
            ]
        }
    ]
}
```

## Re-train Tiny YOLO
So, let's start retraining Tiny YOLO v2. Get the onnx from [our repository](https://gitlab.com/aloha.eu/yolo_test) or from here https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/tiny-yolov2
and put it in the shared data folder, in the subdirectory algorithms.  

You can select the present `Surveillance preset` and then customize the values as you prefer. We will use this setup:  
```
Module selection
  Performance: on
  Training: on
  Security: on
  Parsimonious: on

Dataset
  Dataset name: CUSTOM
  Dataset folder: usecases/COCO_extract_person_car
  Dataset file: usecases/COCO_extract_person_car/annotations.json
  Split on train/validation: 0.7
  Number of classes: 2

Learning settings
  Task type: detection
  Mode: Full
  YOLO From Classifier: off
  Learning rate: 0.0002
  Decay: 0.33
  Iteration: 3
  Epoch wait: 15
  Batch size: 2
  Learning epochs: 25
  Loss function: detection_loss
  Optimizer funcion: sgd
  Accuracy: mAP

Pre-processing pipeline
  The Gray Padding pipeline is ok

Algorithm Parameters
  Select from path -> algorithms/tinyyolo.onnx
  Check the toggle button: Load params from ONNX

Architecture description
  Choose the architecture file we suggest in the next paragraphs

Contraints
  for this tutorial are not important
```

![create1](detection/create1.png)
![create2](detection/create2.png)


## Train YOLO-like models for detection












